import {BrowserModule} from '@angular/platform-browser';
import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {Geolocation} from '@ionic-native/geolocation';

import {HomePage} from '../pages/home/home';
import {HikeDetailsPage} from '../pages/hike-details/hike-details';
import {HikeInProgressPage} from '../pages/hike-inprogress/hike-inprogress';
import {HikeListPage} from '../pages/hike-list/hike-list';
import {Timer} from '../pages/timer/timer';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AgmCoreModule} from '@agm/core';
import {CurrentServiceHikeProvider} from '../providers/current-service-hike/current-service-hike';
import {GeoLocalisationProvider} from '../providers/geo-localisation/geo-localisation';
import {TimerService} from '../providers/TimerService/TimerService';
import {AgmDirectionModule} from "agm-direction";
import {OpenDataServiceProvider} from '../providers/open-data-service/open-data-service';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        HikeDetailsPage,
        HikeInProgressPage,
        HikeListPage,
        Timer
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        AgmDirectionModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDpsLJtvSlShfE-sEfxr_BAjSDLMhp4AtU'
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        HikeDetailsPage,
        HikeInProgressPage,
        HikeListPage,
        Timer
    ],
    providers: [
        CurrentServiceHikeProvider,
        StatusBar,
        SplashScreen,
        TimerService,
        GeoLocalisationProvider,
        Geolocation,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        OpenDataServiceProvider
    ]
})
export class AppModule {
}
