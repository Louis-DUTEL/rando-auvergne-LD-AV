import {Injectable} from '@angular/core';
import {Hike} from "../../model/hike";
import axios from "axios/index";
import {Step} from "../../model/step";

/*
  Generated class for the OpenDataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

/**
 * Service permettant de récuperer une liste de randonnée via une url --> OpenData
 */
@Injectable()
export class OpenDataServiceProvider {

    public _openDataHikes: Array<Hike>;

    constructor() {
        this._openDataHikes = [];
        this.getOpenDataHikes().then((succ: Array<Hike>) => {
            this._openDataHikes = succ;
        });
    }

    getOpenDataHikes() {
        return new Promise((resolve, reject) => {
            axios.get("https://maps.metzmetropole.fr/ows?service=WFS&version=2.0.0&request=GetFeature&typeName=public:pub_tou_bal_nat_rando&srsName=EPSG:4326&outputFormat=json")
                .then((response) => {
                    let hikes = [];

                    response.data.features.forEach(function (hikeOD, index, map) {
                        let hike = null;
                        axios.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + hikeOD.geometry.coordinates[0][0][1] + "," + hikeOD.geometry.coordinates[0][0][0] + "&key=AIzaSyDpsLJtvSlShfE-sEfxr_BAjSDLMhp4AtU")
                            .then((response) => {
                                hike = new Hike("Randonnée du " + response.data.results[0].formatted_address, response.data.results[0].formatted_address, "Aucune description fournie", 0, 0, 4, "",
                                    [], true, hikeOD.geometry.coordinates[0][0][1], hikeOD.geometry.coordinates[0][0][0]);
                                let steps = [];
                                let i = 0;
                                hikeOD.geometry.coordinates[0].forEach(function (coordinates) {
                                    steps.push(new Step(hikeOD.geometry.coordinates[0][0][1], hikeOD.geometry.coordinates[0][0][0], "Etape n°" + i, i));
                                    i++;
                                });
                                hike._steps.push(steps);
                                console.log(hike);
                                hikes.push(hike);
                                if (hikes.length == map.length) {
                                    resolve(hikes);
                                }
                            });
                    });
                }).catch((error) => {
                reject(error);
            });
        });
    }

    get openDataHikes() {
        return this._openDataHikes;
    }
}
