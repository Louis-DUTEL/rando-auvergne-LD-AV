import { Injectable } from '@angular/core';
import {Hike} from "../../model/hike";

/*
  Generated class for the CurrentServiceHikeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

/**
 * Service permettant de stocker la randonnée en cours
 */
@Injectable()
export class CurrentServiceHikeProvider {

    private _currentHike: Hike;

    constructor(){
        this._currentHike = null;
    }

    addHike(hike: Hike){
        this._currentHike = hike;
        this._currentHike.active = true;
    }

    get currentHike(){
        return this._currentHike;
    }

    resetHike(){
        this._currentHike = null;
    }

}
