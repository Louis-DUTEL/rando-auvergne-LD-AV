import {Component} from '@angular/core';
import {TimerService} from '../../providers/TimerService/TimerService';

/**
 * Controller du Timer
 */
@Component({
    selector: 'chronometre',
    templateUrl: 'timer.html'
})
export class Timer {
    private _timer: TimerService;

    constructor(public timer: TimerService) {
        this._timer = timer;
    }
}
