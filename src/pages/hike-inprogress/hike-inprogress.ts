import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AlertController} from 'ionic-angular';

import {Hike} from '../../model/hike';
import {HikeListPage} from '../../pages/hike-list/hike-list';
import {CurrentServiceHikeProvider} from "../../providers/current-service-hike/current-service-hike";
import {GeoLocalisationProvider} from "../../providers/geo-localisation/geo-localisation";

import {TimerService} from '../../providers/TimerService/TimerService';

/**
 * Controller de la page de la randonnée en cours
 */
@Component({
    selector: 'page-hike-inprogress',
    templateUrl: 'hike-inprogress.html',
})
export class HikeInProgressPage {


    private _hikeSelected: Hike;
    private _currentHikeService: CurrentServiceHikeProvider;

    // google maps
    private zoom: number = 16;
    private lat: number;
    private lng: number;
    private dir = undefined;

    private _timerService: TimerService;

    constructor(public navCtrl: NavController, public navParams: NavParams, public currentHike: CurrentServiceHikeProvider, public alertCtrl: AlertController, public geoService: GeoLocalisationProvider, public timerService: TimerService) {
        this._timerService = timerService;
        geoService.getPositionUpdated().subscribe(
            (pos: Position) => {
                this.lat = pos.coords.latitude;
                this.lng = pos.coords.longitude;
            });
        this._hikeSelected = currentHike.currentHike;
        this._currentHikeService = currentHike;
        this.prepareStep();
    }

    deactivateHike(event) {
        this._hikeSelected.active = false;
        this._currentHikeService.resetHike();
        this._timerService.resetTimer();
        this.navCtrl.push(HikeListPage, {});
    }

    private prepareStep() {
        let directionArray = [];
        for (let i = 0; i < this._hikeSelected.steps.length; i++) {
            if (i + 1 < this._hikeSelected.steps.length) {
                let currentInfo = {
                    origin: {
                        lat: this._hikeSelected.steps[i].latitude,
                        lng: this._hikeSelected.steps[i].longitude,
                        mode: 'TRANSIT'
                    },
                    destination: {
                        lat: this._hikeSelected.steps[i + 1].latitude,
                        lng: this._hikeSelected.steps[i + 1].longitude,
                        mode: 'TRANSIT'
                    },
                };
                directionArray.push(currentInfo);
            }
        }
        this.dir = directionArray;
    }


    itemTapped(event, item) {
        this._timerService.startTimer();
        this._timerService.setPlay();
    }
}
