import {Component} from '@angular/core';

import {NavController, NavParams} from 'ionic-angular';

import {HikeInProgressPage} from "../hike-inprogress/hike-inprogress";
import {Hike} from "../../model/hike";
import {CurrentServiceHikeProvider} from "../../providers/current-service-hike/current-service-hike";

import {TimerService} from '../../providers/TimerService/TimerService';

/**
 * Controller de la page d'une randonnée
 */
@Component({
    selector: 'page-hike-details',
    templateUrl: 'hike-details.html'
})
export class HikeDetailsPage {
    private _selectedHike: Hike;
    private _currentHike: CurrentServiceHikeProvider;
    private _timerService: TimerService;


    constructor(public navCtrl: NavController, public navParams: NavParams, public currentHike: CurrentServiceHikeProvider, public timerService: TimerService) {
        this._selectedHike = navParams.get('hike');
        this._currentHike = currentHike;
        this._timerService = timerService;
    }


    activateHike(event, item) {
        console.log("Activation de la Hike :");
        console.log(this._selectedHike.title);
        console.log(this._currentHike);
        this._currentHike.addHike(this._selectedHike);
        console.log(this._currentHike);
        this.navCtrl.push(HikeInProgressPage, {
            hike: this._selectedHike
        });
        this._timerService.startTimer();
        this._timerService.setPlay();
    }

    itemTapped(event, item) {
        this._timerService.startTimer();
        this._timerService.setPlay();
    }

    goToHikeInProgress(event, item) {
        this.navCtrl.push(HikeInProgressPage, {
            hike: this._selectedHike
        });
    }

}
