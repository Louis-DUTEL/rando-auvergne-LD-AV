import {Component} from '@angular/core';
import {TimerService} from '../../providers/TimerService/TimerService';
import {CurrentServiceHikeProvider} from "../../providers/current-service-hike/current-service-hike";
import {NavController, NavParams} from 'ionic-angular';
import {HikeInProgressPage} from "../hike-inprogress/hike-inprogress";
import {HikeListPage} from '../hike-list/hike-list';
import {Hike} from "../../model/hike";

/**
 * Controller de la page Home
 */
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    private _selectedHike: Hike;
    private _timerService: TimerService;
    private _currentHike: CurrentServiceHikeProvider;


    constructor(public navCtrl: NavController, public navParams: NavParams, public timerService: TimerService, public currentHike: CurrentServiceHikeProvider) {
        this._timerService = timerService;
        this._currentHike = currentHike;
        this._selectedHike = navParams.get('hike');
    }

    itemTapped(event, item) {
        this._timerService.startTimer();
        this._timerService.setPlay();
    }

    goToHikeInProgress(event, item) {
        this.navCtrl.push(HikeInProgressPage, {
            hike: this._selectedHike
        });
    }

    goToHikeList(event, item) {
        this.navCtrl.push(HikeListPage);
    }
}
