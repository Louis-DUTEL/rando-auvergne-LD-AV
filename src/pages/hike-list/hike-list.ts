import {Component} from '@angular/core';

import {NavController, NavParams} from 'ionic-angular';

import {HikeDetailsPage} from '../hike-details/hike-details';
import {Hike} from '../../model/hike';
import {Step} from '../../model/step';

import {TimerService} from '../../providers/TimerService/TimerService';

import {CurrentServiceHikeProvider} from "../../providers/current-service-hike/current-service-hike";
import {HikeInProgressPage} from "../hike-inprogress/hike-inprogress";
import {OpenDataServiceProvider} from "../../providers/open-data-service/open-data-service";

/**
 * Controller de la page de la liste des randonnées
 */
@Component({
    selector: 'page-hike-list',
    templateUrl: 'hike-list.html'
})
export class HikeListPage {
    public _hikes: Array<Hike>;
    private _timerService: TimerService;
    private _selectedHike: Hike;
    private _currentHike: CurrentServiceHikeProvider;

    constructor(public navCtrl: NavController, public navParams: NavParams, public timerService: TimerService, public currentHike: CurrentServiceHikeProvider, public openDataService: OpenDataServiceProvider) {
        this._timerService = timerService;
        this._currentHike = currentHike;
        this._selectedHike = navParams.get('hike');
        this._hikes = openDataService.openDataHikes;
        this._hikes.push(new Hike("Rando du Puy de Dôme", "Auvergne - Clermont-Ferrand", "Sympahtique randonée en montagne", 6666, 222, 2, "assets/imgs/puy_de_dome.jpg",
            [new Step(15.165163, 7.65148, "Etape 1", 1), new Step(14.165163, 8.65148, "Etape 2", 2)], false, 15.23515, 5.1653));
        this._hikes.push(new Hike("Rando de Gergovie", "Auvergne - Aubière", "Sympahtique randonée sur le plateau de Gergovie", 5555, 111, 1, "assets/imgs/puy_de_dome.jpg",
            [new Step(15.165163, 7.65148, "Etape 1", 1), new Step(14.165163, 8.65148, "Etape 2", 2)], false, 1.13542, 2.51564));
        this._hikes.push(new Hike("Rando de la Cathédrale Noire", "Auvergne - Clermont-Ferrand", "Sympahtique", 2000, 30, 4, "assets/imgs/puy_de_dome.jpg",
            [new Step(15.165163, 7.65148, "Etape 1", 1), new Step(14.165163, 8.65148, "Etape 2", 2)], false, 9.15648, 25.2364));
    }

    hikeTapped(event, hike) {
        this.navCtrl.push(HikeDetailsPage, {
            hike: hike
        });
    }

    get hikes() {
        return this._hikes;
    }

    itemTapped(event, item) {
        this._timerService.startTimer();
        this._timerService.setPlay();
    }

    goToHikeInProgress(event, item) {
        this.navCtrl.push(HikeInProgressPage, {
            hike: this._selectedHike
        });
    }

    itemTappedFav($event, item, hike) {
        if (hike.favorite) {
            hike.favorite = false;
        } else {
            hike.favorite = true;
        }

    }
}
