/**
 * Model de l'étape d'une randonnée
 */
export class Step {
    private _latitude: number;
    private _longitude: number;
    private _name: string;
    private _numerotation: number;

    constructor(latitude?: number, longitude?: number, name?: string, numerotation?: number) {
        this._latitude = latitude;
        this._longitude = longitude;
        this._name = name;
        this._numerotation = numerotation;

    }

    get latitude(): number {
        return this._latitude;
    }

    set latitude(value: number) {
        this._latitude = value;
    }

    get longitude(): number {
        return this._longitude;
    }

    set longitude(value: number) {
        this._longitude = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get numerotation(): number {
        return this._numerotation;
    }

    set numerotation(value: number) {
        this._numerotation = value;
    }
}
