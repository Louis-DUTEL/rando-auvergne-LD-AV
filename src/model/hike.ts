import {Step} from './step';

/**
 * Model d'une randonnée
 */
export class Hike {
    private _title: string;
    private _adress: string;
    private _description: string;
    private _range: number;
    private _duration: number;
    private _mark: Array<Number>;
    private _urlImage: string;
    private _steps: Array<Step>;
    private _active: boolean;
    private _latitudeDep: number;
    private _longitudeDep: number;
    private _favorite: boolean

    constructor(title?: string, adress?: string, description?: string, range?: number, duration?: number, mark?: number, urlImage?: string, steps?: Array<Step>, active?: boolean, latitudeDep?: number, longitudeDep?: number) {
        this._title = title;
        this._adress = adress;
        this._description = description;
        this._range = range;
        this._duration = duration;
        this._mark = new Array(mark);
        this._urlImage = urlImage;
        this._steps = steps;
        this._active = active;
        this._latitudeDep = latitudeDep;
        this._longitudeDep = longitudeDep;
        this._favorite = false;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get adress(): string {
        return this._adress;
    }

    set adress(value: string) {
        this._adress = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get range(): number {
        return this._range;
    }

    set range(value: number) {
        this._range = value;
    }

    get duration(): number {
        return this._duration;
    }

    set duration(value: number) {
        this._duration = value;
    }

    get mark() {
        return this._mark;
    }


    get urlImage(): string {
        return this._urlImage;
    }

    set urlImage(value: string) {
        this._urlImage = value;
    }

    get active(): boolean {
        return this._active;
    }

    set active(value: boolean) {
        this._active = value;
    }

    get steps(): Array<Step> {
        return this._steps;
    }

    set steps(value: Array<Step>) {
        this._steps = value;
    }

    get latitudeDep(): number {
        return this._latitudeDep;
    }

    set latitudeDep(value: number) {
        this._latitudeDep = value;
    }

    get longitudeDep(): number {
        return this._longitudeDep;
    }

    set longitudeDep(value: number) {
        this._longitudeDep = value;
    }

    get favorite(): boolean {
        return this._favorite;
    }

    set favorite(value: boolean) {
        this._favorite = value;
    }
}
