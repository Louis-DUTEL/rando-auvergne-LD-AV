# Ionic Project
Rando - Auvergne par Louis DUTEL & Adrien VEILLAULT

## Installation

* Cloner le repository git 
* installer les dépendance avec : > npm install
* lancer le serveur avec : > ionic serve

## Lancer sur un téléphone :

* branher le téléphone
* ionic cordova run android

# Travail effectué
Liste des fonctionnalités implémentés :
* Une application qui fonctionne sous android et navigateur
* Listing des randonnées
* Récupération des randonnées via une requête HTTP et parsing des données récupérer
avec notament le parsing des données récupéré
* Détail de chaque randonnée, avec l'affichage de la liste des étapes
* Possibilité de lancer une randonnées (et de ne pas pourvoir en lancer deux en même temps)
* Lancement du timer lors du dépard avec possibilité de mettre en pause celui quand on veux
* Possibilité de stoper la randonnées en cours
* Accessibilité de la randonnée n'importe ou dans l'application grace au Timer
* Un sercice géolocalisation qui fonctionne
* Mettre une randonnée en favoris (mais listing pas disponible)

# Ce qu'il reste à faire
* L'ensemble de la partie test (unitaire et fonctionnel)
* Mise en place de l'intégration continue
* Tracer une randonnée grâce à ses steps
* Gerer les collisions avec les steps des randonnées


# Problèmes rencontrés
## Réalisation des test
* Nous avons essayer de réaliser les tests avec Appium et Protractor. Nous avons d'abord suivi le cours, mais sans sucés. Nous avons rencontrer des erreurs avec Appium nous indiquant qu'il n'arrivait pas à trouver le « ChromeDriverSession ». 
Nous avons cherché dans la doc d'Appium : 
>http://appium.io/docs/en/writing-running-appium/web/chromedriver/
Mais nous n'avons pas pu résoudre le problème. 
Nous avons aussi trouvé plusieurs issues sur github, mais les solutions proposées n'ont pas fonctionné.
Nous suivi les explications de 
>https://www.joshmorony.com/, mais là aussi sans sucés.
Il y a toujours un problème, les tests ne vont jamais jusqu'au bout.
Nous avons passé plus de 6 heures à essayer de résoudre le problème, mais sans succès.

## Autre problèmes ...
* En début de projet, notre application a rencontré un bug qui ne sortait aucune erreur exploitable, nous avons donc du recommencé le projet
* Bon nombre d'erreurs très peu explicite et qui sont donc difficile à résoudre (et parfois un simple cache clear du serveur suffisait ....)




